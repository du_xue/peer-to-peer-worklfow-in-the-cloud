package ncl.ac.uk.duxue.queueDBservices;

public class InputRecord {
	private String identifier;
	private int processID;
	private int taskID;
	private String fromURL;
	private int preProcessID;
	private int preTaskID;
	private String data;
	
	
	public String getIdentifier()
	{
		return identifier;
	}
	public void setIdentifier()
	{
		String processid = ""+preProcessID;
		String taskid = ""+preTaskID;
		identifier = processid+"."+taskid;
	}
	public int getProcessID() 
	{
		return processID;
	}
	public void setProcessID(int processID) 
	{
		this.processID = processID;
	}
	public int getTaskID() 
	{
		return taskID;
	}
	public void setTaskID(int taskID) 
	{
		this.taskID = taskID;
	}
	public String getFromURL() 
	{
		return fromURL;
	}
	public void setFromURL(String fromURL)
	{
		this.fromURL = fromURL;
	}
	public int getPreProcessID() 
	{
		return preProcessID;
	}
	public void setPreProcessID(int preProcessID) 
	{
		this.preProcessID = preProcessID;
	}
	public int getPreTaskID() 
	{
		return preTaskID;
	}
	public void setPreTaskID(int preTaskID)
	{
		this.preTaskID = preTaskID;
	}
	public String getData() 
	{
		return data;
	}
	public void setData(String data)
	{
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "InputRecord [identifier=" + identifier + ", processID="
				+ processID + ", taskID=" + taskID + ", fromURL=" + fromURL
				+ ", preProcessID=" + preProcessID + ", preTaskID=" + preTaskID
				+ ", data=" + data + "]";
	}

	

}
