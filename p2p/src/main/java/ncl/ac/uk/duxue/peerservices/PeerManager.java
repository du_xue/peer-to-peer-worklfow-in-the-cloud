package ncl.ac.uk.duxue.peerservices;

import java.io.IOException;
import java.lang.reflect.Type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import java.util.concurrent.TimeUnit;


import ncl.ac.uk.duxue.Entities.Event;

import ncl.ac.uk.duxue.Entities.Identifier;
import ncl.ac.uk.duxue.Entities.PeerIdentifier;
import ncl.ac.uk.duxue.Entities.Queue;
import ncl.ac.uk.duxue.Entities.Sent;
import ncl.ac.uk.duxue.Entities.SentSide;
import ncl.ac.uk.duxue.Entities.Workload;
import ncl.ac.uk.duxue.Entities.WrappedTask;
import ncl.ac.uk.duxue.workflowservices.*;
import ncl.ac.uk.duxue.workflowservices.Process;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


public class PeerManager {
	Gson gson = new Gson();
	static private String originalURL;
	Queue backupQueue = new Queue();
	
	List<Task> taskList = new ArrayList<Task>();
	//List<Peer> buddyList = new ArrayList<Peer>();
	static List<Peer> buddyList = new ArrayList<Peer>();
	int hop;
	
	Map<Identifier,List<Peer>> multiMap = new HashMap<Identifier,List<Peer>>();// the peer with the required type according to the task
	Map<String,String> agreedPeer = new HashMap<String, String>();// itemID of peer can receive a task
	
	Type collectionType = new TypeToken<List<Peer>>(){}.getType();// get the Gson type of List<peer>
	
	Process process = null;
	
	public String changeJsontoObjec(Process process) throws IOException, ClassNotFoundException, InterruptedException
	{
		this.process = process;
		System.out.print(process.getTaskList());
		taskList = process.getTaskList();
		/*
		 * scan the tasks, request for same type peer information, store the "taskID" and "worker peer information" inside the peerList
		 */
		for (Task task : process.getTaskList())
		{ 
			String type = task.getType();
			
			
			/*String value = searchPeer(type); //get the peer information from the infopeer search service
			
			List<Peer> peerList = gson.fromJson(value, collectionType);*/
			List<Peer> peerList = new ArrayList<Peer>();
			// Visit the buddylist to get the peer information( peerID and groupID)
						for (Peer p : buddyList)
						{
							if(p.getType().equals(type))
							{
								peerList.add(p);
							}
						}
			Identifier identifier = new Identifier();
			identifier.setId(task.getTaskID());
			identifier.setType(task.getType());
			multiMap.put(identifier, peerList);
			System.out.println("map content:" + multiMap);
			
		}
		String status = null;
	    status = sendMessage(multiMap);
		
		return  status;
	}
    public String secondLevelManager(ArrayList<Task> secondTasklist) throws IOException, ClassNotFoundException, InterruptedException
    {   	
    	
		/*
		 * scan the tasks, request for same type peer information, store the "taskID" and "worker peer information" inside the peerList
		 */
		for (Task task : secondTasklist)
		{ 
			String type = task.getType();
			
			
			/*String value = searchPeer(type); //get the peer information from the infopeer search service
			
			List<Peer> peerList = gson.fromJson(value, collectionType);*/
			List<Peer> peerList = new ArrayList<Peer>();
			// Visit the buddylist to get the peer information( peerID and groupID)
						for (Peer p : buddyList)
						{
							if(p.getType().equals(type))
							{
								peerList.add(p);
							}
						}
			Identifier identifier = new Identifier();
			identifier.setId(task.getTaskID());
			identifier.setType(task.getType());
			multiMap.put(identifier, peerList);
			System.out.println("map content:" + multiMap);
			
		}
		String status = null;
	    status = sendMessage(multiMap);
		
		return  status;
    }
	/*
	 *  according to the peer type search the relevant peer
	 */
	public String searchPeer(String type) throws IOException, ClassNotFoundException
	{
		
		Sent client = new Sent("/infopeer/search/" + type);
		String peerinfo = client.clientStart();
		//String result = (String) Base64.decodeToObject(peerinfo);
		return peerinfo;
	}
	
	/*
	 * send message to worker peer 
	 */
	
	public String sendMessage(Map<Identifier,List<Peer>> multiMap) throws IOException, ClassNotFoundException, InterruptedException
	{
		
		int statusInt =0;
		String confirmedURL = null;

		
		for (Entry<Identifier, List<Peer>> entry : multiMap.entrySet()) {
			Queue peerQueue =  new Queue();
			int flag = 0;
			/////////////////////////////////////////////////////////
			if(!(backupQueue.getList().isEmpty()))
			{
				confirmedURL = null;
				System.out.println("not empty !!!!!!!!");
				for(Event w : backupQueue.getList())
				{
					Workload ww =  (Workload) w;
					if(entry.getKey().getType().equals(ww.getPeerRecord().getType()))
					{
						confirmedURL = ww.getPeerRecord().getURL();
						System.out.println("agreedPeer put Key : " + entry.getKey().getId() + "  Value : " + confirmedURL);
						agreedPeer.put(entry.getKey().getId(), confirmedURL);
						//iter.remove();
						backupQueue.remove(w);
						statusInt = (int) ww.getEventTime();
						ww.setEventTime(statusInt +1);
						backupQueue.add(ww);
						flag = 0;
						break;
						
					}
					else
					{
						flag = 1;
					}
				}
				if(flag ==1)
				{
					///////////////////////
					
						confirmedURL = null;
						List<Peer> peerAddressList =  new ArrayList<Peer>();
						System.out.println("Key : " + entry.getKey() + " Value : "
							+ entry.getValue());
						if (entry.getValue().isEmpty()) {

							hop++;
							String value = searchPeer(entry.getKey().getType()); 

							List<Peer> peerList = gson.fromJson(value, collectionType);
							if (peerList.isEmpty()) {

								String out = searchPeer("infopeer");
								List<Peer> infopeerList = gson
										.fromJson(out, collectionType);
								for (Peer infopeer : infopeerList) {
									hop++;
									String infoURL = infopeer.getUrl();
									SentSide client = new SentSide(infoURL
											+ "/infopeer/search/"
											+ entry.getKey().getType());
									String outsidepeer = client.clientStart();
									List<Peer> outsidePeerList = gson.fromJson(outsidepeer,
											collectionType);
									if (!outsidePeerList.isEmpty()) {
										peerAddressList = outsidePeerList;
										break;
									}
								}
							}
							else {
								peerAddressList = peerList;
							}

						}

						else {
							peerAddressList = entry.getValue();
						}
				     	//List<String> okPeer = new ArrayList<String>();
						for(Peer peer : peerAddressList)
						{
							
								String address, status = null;
							    address = peer.getUrl();
								System.out.println("address:!" + address);
								
					//***********implement send message to worker peer, if they can accept the request****************
								SentSide client = new SentSide( address+"/peer/message");
								status = client.clientStart();
								System.out.println("result" + status);
								// convert String workload to Integer
								statusInt = Integer.parseInt(status.toString());
								
								PeerIdentifier peerInWorkload = new PeerIdentifier();
								peerInWorkload.setType(peer.getType());
								peerInWorkload.setURL(peer.getUrl());
								
							//put the workload in the queue to chose the best peer
							Workload workload = new Workload(peerInWorkload, statusInt);
							peerQueue.add(workload);// peerQueue contains the peer with the decent workload
						}	
						if(!peerQueue.isEmpty())
						{
					    Workload first = (Workload) peerQueue.popNext();
					    System.out.println("pop in agreePeer is " + first.toString());
					    confirmedURL = first.getPeerRecord().getURL();
					    System.out.println("Confirmed URL is : " + confirmedURL);
					    statusInt ++;
					    first.setEventTime(statusInt);
					    backupQueue.add(first);
						}
						while(!peerQueue.isEmpty())
						{
							Workload backup = (Workload) peerQueue.popNext();
							backupQueue.add(backup);
						}
						System.out.println("back up Queue is " + backupQueue.getList().toString());
					    System.out.println("agreedPeer put Key : " + entry.getKey().getId() + "  Value : " + confirmedURL);
						agreedPeer.put(entry.getKey().getId(), confirmedURL);
					}
					/////////////////
				
			}
			////////////////////////////////////////////////////////	
			else{
				confirmedURL = null;
			List<Peer> peerAddressList =  new ArrayList<Peer>();
			System.out.println("Key : " + entry.getKey() + " Value : "
				+ entry.getValue());
			if (entry.getValue().isEmpty()) {

				hop++;
				String value = searchPeer(entry.getKey().getType()); 

				List<Peer> peerList = gson.fromJson(value, collectionType);
				if (peerList.isEmpty()) {

					String out = searchPeer("infopeer");
					List<Peer> infopeerList = gson
							.fromJson(out, collectionType);
					for (Peer infopeer : infopeerList) {
						hop++;
						String infoURL = infopeer.getUrl();
						SentSide client = new SentSide(infoURL
								+ "/infopeer/search/"
								+ entry.getKey().getType());
						String outsidepeer = client.clientStart();
						List<Peer> outsidePeerList = gson.fromJson(outsidepeer,
								collectionType);
						if (!outsidePeerList.isEmpty()) {
							peerAddressList = outsidePeerList;
							break;
						}
					}
				}
				else {
					peerAddressList = peerList;
				}

			}

			else {
				peerAddressList = entry.getValue();
			}
	     	//List<String> okPeer = new ArrayList<String>();
			for(Peer peer : peerAddressList)
			{
				
					String address, status = null;
				    address = peer.getUrl();
					System.out.println("address:!" + address);
					
		//***********implement send message to worker peer, if they can accept the request****************
					SentSide client = new SentSide( address+"/peer/message");
					status = client.clientStart();
					System.out.println("result" + status);
					// convert String workload to Integer
					statusInt = Integer.parseInt(status.toString());
					
					PeerIdentifier peerInWorkload = new PeerIdentifier();
					peerInWorkload.setType(peer.getType());
					peerInWorkload.setURL(peer.getUrl());
					
				//put the workload in the queue to chose the best peer
				Workload workload = new Workload(peerInWorkload, statusInt);
				peerQueue.add(workload);// peerQueue contains the peer with the decent workload
			}	
			if(!peerQueue.isEmpty())
			{
		    Workload first = (Workload) peerQueue.popNext();
		    System.out.println("pop in agreePeer is " + first.toString());
		    confirmedURL = first.getPeerRecord().getURL();
		    System.out.println("Confirmed URL is : " + confirmedURL);
		    statusInt ++;
		    first.setEventTime(statusInt);
		    backupQueue.add(first);
			}
			while(!peerQueue.isEmpty())
			{
				Workload backup = (Workload) peerQueue.popNext();
				backupQueue.add(backup);
			}
			System.out.println("back up Queue is " + backupQueue.getList().toString());
		    System.out.println("agreedPeer put Key : " + entry.getKey().getId() + "  Value : " + confirmedURL);
			agreedPeer.put(entry.getKey().getId(), confirmedURL);
		
			}
		   
		}	
		System.out.println("agreedPeer!" + agreedPeer);
		System.out.println("****************");
		System.out.println("* Total HOP = " + hop +"*");
		System.out.println("****************");
		String status = null;
		status = sendWrappedTask(agreedPeer, taskList, process );
		return status;
	}
	
	/*
	 * wrap the task before sending
	 */
	public String sendWrappedTask(Map<String,String> agreedPeer, List<Task> taskList, Process process ) throws IOException, InterruptedException
	{
		ExecutorService executor = Executors.newFixedThreadPool(15);
		//ScheduledExecutorService canceller = Executors.newSingleThreadScheduledExecutor();
		//String string = null;
		String result= null;
		originalURL = agreedPeer.get("1");
		System.out.println("******* Start sending wrapped Task**********");
		long startTime = System.currentTimeMillis();
		for(Task task : taskList)
		{
			if(agreedPeer.get(task.getTaskID()) != null)
			{
				String url = agreedPeer.get(task.getTaskID());
				WrappedTask wrappedTask =  new WrappedTask();
			    wrappedTask.setTask(task);
			    wrappedTask.setProcessID(process.getProcessID());
			    
			    wrappedTask.setOriginalURL(originalURL);
			    //set from URL
				for (TaskIdentifier identifier : task.getPre().getPreList()) {
					if ((identifier.getTaskID()).equals("0")) {
						wrappedTask.addFromURL("end");

					} else {
						wrappedTask.addFromURL(agreedPeer.get(identifier
								.getTaskID()));// according to the "agreedPeer"
												// get the working peer
					}
				}
				//set to URL
				for (TaskIdentifier identifier : task.getPose().getPoseList()) {

					if ((identifier.getTaskID()).equals("0")) {
						wrappedTask.addToURL("end");
					} else {
						wrappedTask.addToURL(agreedPeer.get(identifier
								.getTaskID()));// according to the "agreedPeer"
												// get the working peer
					}
				}
				System.out.println(url + "%#");
				System.out.println(wrappedTask);
				
				/*ClientConfig config = new DefaultClientConfig();
				Client client = Client.create(config);
				System.out.println("***********");
				WebResource service = client.resource(UriBuilder.fromUri("http://"+url).build());
				ArrayList<TaskIdentifier> preList = (ArrayList<TaskIdentifier>) wrappedTask.getTask().getPre().getPreList();
				ArrayList<TaskIdentifier> poseList = (ArrayList<TaskIdentifier>) wrappedTask.getTask().getPose().getPoseList();
				System.out.println("^^^^^^^^^^^");
				
				task.getPre().setPreList(null);
				task.getPose().setPoseList(null);
				
				String jsonTask = gson.toJson(task);
				
				String jsonPreList = gson.toJson(preList);
				String jsonPoseList = gson.toJson(poseList);
				
				String fromURLJson = gson.toJson(wrappedTask.getFromURL());
				String toURLJson = gson.toJson(wrappedTask.getToURL());
				System.out.println("!!!!!!!!!");
				
				Form f = new Form(); 
				f.add("processid", wrappedTask.getProcessID());
				f.add("fromurl", fromURLJson);
				f.add("tourl", toURLJson);
				f.add("originalurl", originalURL);
				f.add("task", jsonTask);
				f.add("prelist", jsonPreList);
				f.add("poselist", jsonPoseList);
				
                result = service.path("peer").path("wrappedTask").post(String.class,f);
				
				System.out.println("result" + result);
				System.out.println(agreedPeer.get(task.getTaskID()));*/
				
				
				Runnable worker = new SendWrappedTask(url, wrappedTask, task);
				Thread.sleep(3000);
				final Future<?> future = executor.submit(worker);
				//executor.execute(worker);
				//canceller.schedule(worker, 40, TimeUnit.SECONDS);
				try {
					future.get(10000, TimeUnit.SECONDS);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					future.cancel(true);
				} 
			
			}
		}
		executor.shutdown();
		
		while (!executor.isTerminated()) {
			
			
		}
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("The Execution time time is" + totalTime + "millinsecond");
		return result;
		
	}

}
