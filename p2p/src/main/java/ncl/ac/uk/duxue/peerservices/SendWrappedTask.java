package ncl.ac.uk.duxue.peerservices;

import java.util.ArrayList;

import javax.ws.rs.core.UriBuilder;

import ncl.ac.uk.duxue.Entities.WrappedTask;
import ncl.ac.uk.duxue.workflowservices.Task;
import ncl.ac.uk.duxue.workflowservices.TaskIdentifier;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;


public class SendWrappedTask implements Runnable{
     Gson gson = new Gson();
     String url;
     WrappedTask wrappedTask =  new WrappedTask();
     Task task;
     String originalURL;
     String result;
     
     SendWrappedTask(String url, WrappedTask wrappedTask, Task task)
     {
    	 this.url= url;
    	 this.wrappedTask = wrappedTask;
    	 this.task = task;
    	 
     }
	
	@Override
	public void run()
	{
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		System.out.println("***********");
		WebResource service = client.resource(UriBuilder.fromUri("http://"+url).build());
		ArrayList<TaskIdentifier> preList = (ArrayList<TaskIdentifier>) wrappedTask.getTask().getPre().getPreList();
		ArrayList<TaskIdentifier> poseList = (ArrayList<TaskIdentifier>) wrappedTask.getTask().getPose().getPoseList();
		System.out.println("^^^^^^^^^^^");
		
		task.getPre().setPreList(null);
		task.getPose().setPoseList(null);
		
		String jsonTask = gson.toJson(task);
		
		String jsonPreList = gson.toJson(preList);
		String jsonPoseList = gson.toJson(poseList);
		
		String fromURLJson = gson.toJson(wrappedTask.getFromURL());
		String toURLJson = gson.toJson(wrappedTask.getToURL());
		System.out.println("!!!!!!!!!");
		
		Form f = new Form(); 
		f.add("processid", wrappedTask.getProcessID());
		f.add("fromurl", fromURLJson);
		f.add("tourl", toURLJson);
		f.add("originalurl", originalURL);
		f.add("task", jsonTask);
		f.add("prelist", jsonPreList);
		f.add("poselist", jsonPoseList);
		
        result = service.path("peer").path("wrappedTask").post(String.class,f);

		
		System.out.println("result" + result);
		
	}
	public String backResult()
	{
		return result;
	}
}
