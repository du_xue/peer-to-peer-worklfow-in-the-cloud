package ncl.ac.uk.duxue.Entities;

public class PeerIdentifier {

	private String URL;
	private String type;
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "PeerIdentifier [URL=" + URL + ", type=" + type + "]";
	}
	
	

}
