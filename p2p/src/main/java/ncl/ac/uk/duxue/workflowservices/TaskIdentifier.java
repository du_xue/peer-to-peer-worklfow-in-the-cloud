package ncl.ac.uk.duxue.workflowservices;

public class TaskIdentifier {
	private String processId;
	private String taskID;

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getTaskID() {
		return taskID;
	}

	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

	@Override
	public String toString() {
		return "TaskIdentifier [processId=" + processId + ", taskID=" + taskID
				+ "]";
	}

}
