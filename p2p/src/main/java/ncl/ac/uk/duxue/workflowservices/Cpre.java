package ncl.ac.uk.duxue.workflowservices;

import java.util.ArrayList;
import java.util.List;

public class Cpre {
	private List<TaskIdentifier> preList = new ArrayList<TaskIdentifier>();
	private String logic;
	
	public Cpre()
	{
		
	}

	public List<TaskIdentifier> getPreList() {
		return preList;
	}

	public void setPreList(List<TaskIdentifier> preList) {
		this.preList = preList;
	}

	public String getLogic() {
		return logic;
	}

	public void setLogic(String logic) {
		this.logic = logic;
	}
    public void addPre(TaskIdentifier task)
    {
    	preList.add(task);
    }
	
	@Override
	public String toString() {
		return "Cpre [preList=" + preList + ", logic=" + logic + "]";
	}		
}
