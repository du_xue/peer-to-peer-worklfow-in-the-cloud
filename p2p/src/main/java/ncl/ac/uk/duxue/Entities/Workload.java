package ncl.ac.uk.duxue.Entities;


public class Workload extends Event{
	
	private PeerIdentifier peer;
	
	public Workload(PeerIdentifier peer, int workload)
	{
		this.peer= peer;
		super.setEventTime(workload);
	}
	
	public PeerIdentifier getPeerRecord()
	{
		return peer;
	}

}
