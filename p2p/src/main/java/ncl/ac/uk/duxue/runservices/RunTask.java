package ncl.ac.uk.duxue.runservices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class RunTask {

	public void start() throws IOException, InterruptedException {

		InputStream inputStream = null;
		OutputStream outputStream = null;
		
	    File f = new File("G:\\p2p");
	    f.mkdirs();
	    File fexe = new File("G:\\p2p\\writefile.exe");
	    fexe.createNewFile();
	    //f.createNewFile();
		try {
			// read this file into InputStream
			Thread.sleep(4000);
			inputStream = RunTask.class.getResourceAsStream("/writefile.exe");
	 
			// write the inputStream to a FileOutputStream
			outputStream = 
	                    new FileOutputStream(fexe);
	 
			int read = 0;
			byte[] bytes = new byte[1024];
	 
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
	 
			System.out.println("Done!");
	 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
	 
			}
		}
		Thread.sleep(4000);
		Runtime runtime = Runtime.getRuntime();
		
		runtime.exec("G:\\p2p\\writefile.exe", null, new File("G:\\p2p"));		
	}

}
