package ncl.ac.uk.duxue.peerservices;

import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.UriBuilder;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;

import ncl.ac.uk.duxue.Entities.Message;
import ncl.ac.uk.duxue.Entities.QueueTask;
import ncl.ac.uk.duxue.Entities.WrappedTask;
import ncl.ac.uk.duxue.runservices.RunTask;
import ncl.ac.uk.duxue.workflowservices.TaskIdentifier;

public class Worker {
	static String peerName = "Local Peer";
    //static String peerName = "PEER2";
	//static String peerName = "PEER3";
	//static String peerName = "PEER4";
	//static String peerName = "PEER5";
	//static String peerName = "PEER6";
	//static String peerName = "PEER7";
	
	static String myURL = "localhost:808/p2p";
    // static String myURL = "env-4111235.j.layershift.co.uk/peer2";
	//static String myURL = "env-6473475.j.layershift.co.uk/peer3";
	//static String myURL = "peer4.elasticbeanstalk.com";
	//static String myURL = "peer5.elasticbeanstalk.com";
	//static String myURL = "peer6.elasticbeanstalk.com";
	//static String myURL = "peer7.elasticbeanstalk.com";
	
	int maxSize;
	int size;

	static QueueTask queue;
	// static Queue queue = new Queue();
	static Map<String, String> inputMap = new HashMap<String, String>();
	static Map<String, String> outPutMap = new HashMap<String, String>();
	Gson gson = new Gson();
	String status;
	int sleep;
    private static Worker Instance = null;
		 
	public static Worker getInstance() { 
		  if(Instance == null) {
		         Instance = new Worker();
		         
		      }
	    return Instance;
	  }

	public Worker() {
		queue = QueueTask.getInstance();
		maxSize = 0;
		size = 0;
		status = null;
	}

	public String onWork(WrappedTask wrapped) throws InterruptedException,
			IOException {
		getInputMap(wrapped);
		getOutputMap(wrapped);

		/*for (Entry<String, String> entry : inputMap.entrySet()) {
			System.out.println("Received===>Worker " + peerName + " "
					+ "Key : " + entry.getKey() + " Value : "
					+ entry.getValue());
		}*/

		long val = System.currentTimeMillis();
		wrapped.setEventTime(val);
		System.out.println(val);

		queue.add(wrapped);
		sleep ++;
	
		// "Add task:"+"ProcessID: "+wrapped.getProcessID()+" TaskID: "+
		// wrapped.getTask().getTaskID();
		//System.out.println("This is Queue size of " + peerName + " After add:"
				//+ queue.getSize());
		status = "\n" + "The max queue size is :" + maxSize();
		//Thread.sleep(5000);
		while (queue.getSize() != 0) {
			Thread.sleep(8000);
			WrappedTask pop = (WrappedTask) queue.popNext();
			  
			   int flag = checkInputMap(pop);
             /*  System.out.println("flag : " + flag);
               System.out.println("ArraySize : " + pop.getFromURL().size());
               System.out.println("Logic : " + pop.getTask().getPre().getLogic());*/
			if (pop.getTask().getPre().getLogic().equals("and")) {
				if (flag == pop.getFromURL().size()) {
	
					System.out.println("ProcessID: " + pop.getProcessID()
							+ " TaskID: " + pop.getTask().getTaskID()
							+ " ==>inside ownpeer1 Working!!!");

					status = status + "\n" + "ProcessID: " + pop.getProcessID()
							+ " TaskID: " + pop.getTask().getTaskID()
							+ " ==>inside " + peerName + "!!!";
					
					RunTask runTask = new RunTask();
					runTask.start();
					Thread.sleep(10000*queue.getSize());
					SimpleDateFormat   sDateFormat   =   new   SimpleDateFormat("yyyy-MM-dd   hh:mm:ss");     
					String   date   =   sDateFormat.format(new   java.util.Date()); 
					status = status + "\n" + date;

					// send the message to the other peer
					for (String tourl : pop.getToURL()) {
						if (!tourl.equals("end")) {
							System.out.println("Send Message!");
							sendMessage(
									Integer.parseInt(pop.getProcessID()),
									Integer.parseInt(pop.getTask().getTaskID()),
									"enable", tourl);
							status = status + "\n" + peerName + " Finished!";
						} else {
							//Thread.sleep(5000);
							status = status + "\n" + peerName + " Finished!";
						}
					}
				} else {
					Thread.sleep(5000);
					long updateVal = System.currentTimeMillis();
					pop.setEventTime(updateVal);
					System.out.println("@update time@" + updateVal);
					queue.add(pop);
					maxSize();
				}
			}

			else /*if (pop.getTask().getPre().getLogic().equals("or")
					&& pop.getTask().getPre().getLogic().equals("straight"))*/ {

				if (flag >= 1) {
			
					System.out.println("ProcessID: " + pop.getProcessID()
							+ " TaskID: " + pop.getTask().getTaskID()
							+ " ==>inside ownpeer1 Working!!!");

					status = status + "\n" + "ProcessID: " + pop.getProcessID()
							+ " TaskID: " + pop.getTask().getTaskID()
							+ " ==>inside " + peerName + "!!!";
					RunTask runTask = new RunTask();
					runTask.start();
					Thread.sleep(8000*queue.getSize());
					SimpleDateFormat   sDateFormat   =   new   SimpleDateFormat("yyyy-MM-dd   hh:mm:ss");     
					String   date   =   sDateFormat.format(new   java.util.Date()); 
					status = status + "\n" + date;
					// send the message to the other peer
					for (String tourl : pop.getToURL()) {
						if (!tourl.equals("end")) {
							sendMessage(
									Integer.parseInt(pop.getProcessID()),
									Integer.parseInt(pop.getTask().getTaskID()),
									"enable", tourl);
						} else {
							//Thread.sleep(5000);
							status = status + "\n" + peerName + " Finished!";
						}
					}
				} else {
					Thread.sleep(5000);
					long updateVal = System.currentTimeMillis();
					pop.setEventTime(updateVal);
					System.out.println("@update time@" + updateVal);
					queue.add(pop);
					maxSize();
				}
			}

		}

		return status;
	}

	public void receiveMessage(Message message) {

		String processid = "" + message.getProcessID();
		String taskid = "" + message.getTaskID();
		String identifier = processid + "." + taskid;
        String input = message.getData();
		inputMap.put(identifier, input);
	/*	for (Entry<String, String> entry : inputMap.entrySet()) {
			System.out.println("Received===>after receive message  " + "Key : "
					+ entry.getKey() + " Value : " + entry.getValue());
		}*/
	}

	public void sendMessage(int processID, int taskID, String data, String toURL)
			throws IOException {
		Message message = new Message();
		message.setProcessID(processID);
		message.setTaskID(taskID);
		message.setData(data);
		if (toURL.equals(myURL)) {
			receiveMessage(message);
		} else {
			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);
			System.out.println("@@@@@@@@@@");
			WebResource service = client.resource(UriBuilder.fromUri(
					"http://" + toURL).build());
			System.out.println("###########");
			Form f = new Form();
			f.add("processid", processID);
			f.add("taskid", taskID);
			f.add("data", data);
			System.out.println("Form" + f);
			service.path("peer").path("datamessage").post(String.class, f);
		}
	}

	public int getSize() {
		int status;
		size = queue.getSize();
		System.out.println("static Size1===>" + size);
		switch (size) {
		case 0:
			status = 0;
			break;
		case 1:
			status = 20;
			break;
		case 2:
			status = 40;
			break;
		case 3:
			status = 60;
			break;
		case 4:
			status = 80;
			break;
		case 5:
			status = 100;
		default:
			status = 100;
			break;
		}
		return status;
	}

/*	public InputRecord createInputRecord(WrappedTask task) {
		InputRecord inRecord = new InputRecord();
		inRecord.setProcessID(Integer.parseInt(task.getProcessID()));
		inRecord.setTaskID(Integer.parseInt(task.getTask().getTaskID()));
		inRecord.setFromURL(task.getFromURL());
		inRecord.setPreProcessID(Integer.parseInt(task.getTask().getPre()
				.getProcessID()));
		inRecord.setPreTaskID(Integer.parseInt(task.getTask().getPre()
				.getTaskID()));

		inRecord.setIdentifier();

		return inRecord;
	}

	public OutputRecord createOutputRecord(WrappedTask task) {
		OutputRecord outRecord = new OutputRecord();
		outRecord.setProcessID(Integer.parseInt(task.getProcessID()));
		outRecord.setTaskID(Integer.parseInt(task.getTask().getTaskID()));
		outRecord.setToURL(task.getFromURL());
		outRecord.setPoseProcessID(Integer.parseInt(task.getTask().getPre()
				.getProcessID()));
		outRecord.setPoseTaskID(Integer.parseInt(task.getTask().getPre()
				.getTaskID()));
		outRecord.setData(null);

		return outRecord;
	}*/

	public void getInputMap(WrappedTask task) {
		for (TaskIdentifier ti : task.getTask().getPre().getPreList()) {
			String processID = ti.getProcessId();
			String taskID = ti.getTaskID();
			String identifier = processID + "." + taskID;
			if (inputMap.get(identifier) == null) {
				inputMap.put(identifier, null);
			}
		}
	}

	public void getOutputMap(WrappedTask task) {
	
	}

	public int checkInputMap(WrappedTask task) {
		int flag = 0;
		for (TaskIdentifier ti : task.getTask().getPre().getPreList()) {
			String processID = ti.getProcessId();
			String taskID = ti.getTaskID();
			String identifier = processID + "." + taskID;

			String input = inputMap.get(identifier);
			System.out.println(identifier);

			if (identifier.equals("0.0")) {
				flag++;
			} else if (input == null) {

			} else if (input.equals("enable")) {
				flag++;
			} else {

			}
		}
		return flag;
	}

	public int maxSize() {
		if (maxSize < queue.getSize()) {
			maxSize = queue.getSize();
		}
		return maxSize;
	}
}
