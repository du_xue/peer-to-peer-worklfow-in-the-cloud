package ncl.ac.uk.duxue.queueDBservices;

public class QTask {
	private int uniqueID;
	private int taskID;
	private int processID;
	private int preProcessID;
	private int preTaskID;
	private int poseProcessID;
	private int poseTaskID;
	private String originalURL;
	private String fromURL;
	private String toURL;
	private String script;
	private long enterTime;
	
	
	public int getUniqueID() 
	{
		return uniqueID;
	}
	public void setUniqueID() 
	{
		String q = "" + processID;
		String p = "" + taskID;
		uniqueID = Integer.parseInt(q + p);
	}
	public int getTaskID() {
		return taskID;
	}
	public void setTaskID(int taskID) 
	{
		this.taskID = taskID;
	}
	public int getProcessID() 
	{
		return processID;
	}
	public void setProcessID(int processID) 
	{
		this.processID = processID;
	}
	
	public int getPreProcessID() 
	{
		return preProcessID;
	}
	public void setPreProcessID(int preProcessID) 
	{
		this.preProcessID = preProcessID;
	}
	public int getPreTaskID() 
	{
		return preTaskID;
	}
	public void setPreTaskID(int preTaskID) 
	{
		this.preTaskID = preTaskID;
	}
	public int getPoseProcessID() 
	{
		return poseProcessID;
	}
	public void setPoseProcessID(int poseProcessID) 
	{
		this.poseProcessID = poseProcessID;
	}
	public int getPoseTaskID() 
	{
		return poseTaskID;
	}
	public void setPoseTaskID(int poseTaskID) 
	{
		this.poseTaskID = poseTaskID;
	}
	public void setUniqueID(int uniqueID) 
	{
		this.uniqueID = uniqueID;
	}
	public String getOriginalURL() 
	{
		return originalURL;
	}
	public void setOriginalURL(String originalURL) 
	{
		this.originalURL = originalURL;
	}
	public String getFromURL() 
	{
		return fromURL;
	}
	public void setFromURL(String fromURL) 
	{
		this.fromURL = fromURL;
	}
	public String getToURL() 
	{
		return toURL;
	}
	public void setToURL(String toURL) 
	{
		this.toURL = toURL;
	}
	public String getScript() 
	{
		return script;
	}
	public void setScript(String script)
	{
		this.script = script;
	}
	public long getEnterTime() 
	{
		return enterTime;
	}
	public void setEnterTime(long enterTime) 
	{
		this.enterTime = enterTime;
	}
	@Override
	public String toString() {
		return "QueueTask [uniqueID=" + uniqueID + ", taskID=" + taskID
				+ ", processID=" + processID + ", preProcessID=" + preProcessID
				+ ", preTaskID=" + preTaskID + ", poseProcessID="
				+ poseProcessID + ", poseTaskID=" + poseTaskID
				+ ", originalURL=" + originalURL + ", fromURL=" + fromURL
				+ ", toURL=" + toURL + ", script=" + script + ", enterTime="
				+ enterTime + "]";
	}
}
