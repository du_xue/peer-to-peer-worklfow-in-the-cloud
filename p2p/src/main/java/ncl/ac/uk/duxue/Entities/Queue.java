package ncl.ac.uk.duxue.Entities;

import java.util.ArrayList;
import java.util.Iterator;



public class Queue {

	  // The queue of events
	  private ArrayList<Event> queue = new ArrayList<Event>();
	  
	  // Add a new element to the list
	  public boolean add(Event e) {
	    if (queue.isEmpty()) {
		  queue.add(e);
		  return true;
		}
		int pos = 0;
		Iterator<Event> i = queue.iterator();
		while (i.hasNext()) {
		  Event c = (Event) i.next();
		  if (c.getEventTime() > e.getEventTime()) {
		    queue.add(pos, e);
			return true;
		  }
		  pos ++;
		}
		// end of list reached so add to end
		queue.add(e);
		return true;
	  }
	  
	  public boolean remove(Event e) {
	    return queue.remove(e);
	  }
	  
	  public Event popNext() {
	    if (queue.isEmpty())
	    {
		  System.out.println("ERROR: Empty queue!");
		  return null;
	    }
	    Event ret = (Event) queue.get(0);
		queue.remove(ret);
		return ret;
	  }
	  public boolean isEmpty()
		{
			return queue.isEmpty();
		}
	  public int getSize()
	  {
		  int size = queue.size();
		  return size;
	  }
	  public void removeAllEvent()
	  {
		  queue.removeAll(queue);
	  }
	  public ArrayList<Event> getList()
	  {
		  return queue;
	  }
	  public boolean isnotEmpty()
	  {
		  if(queue.size()>=1)
		  {
		  return true;
		  }
		  else{
		  return false;
		  }
	  }
}
