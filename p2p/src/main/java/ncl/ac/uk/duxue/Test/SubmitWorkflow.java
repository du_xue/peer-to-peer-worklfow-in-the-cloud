package ncl.ac.uk.duxue.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SubmitWorkflow {

	/**
	 * submit a work flow to local host
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		long startTime = System.currentTimeMillis();
		ExecutorService executor = Executors.newFixedThreadPool(15);

		for (int i = 1; i < 2; i++) {

			Runnable worker = new EStest("" + i);
			Thread.sleep(1000);

			executor.execute(worker);

		}

		executor.shutdown();

		while (!executor.isTerminated()) {

		}
        
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		
		System.out.println("Finished all threads");
		System.out.println("The total execution time : " + totalTime + " milliseconds");

	}
}
