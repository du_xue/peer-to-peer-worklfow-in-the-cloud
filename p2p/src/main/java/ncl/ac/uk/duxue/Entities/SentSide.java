package ncl.ac.uk.duxue.Entities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SentSide {
	private String URL;
	private String result;
	
	public SentSide(String URL)
	{
		this.URL = URL;
	}
	
	public String clientStart()
	{
		try {

			//URL url = new URL("http://env-8762623.j.layershift.co.uk/peer1" + URL);
			//URL url = new URL("http://localhost:8080/p2p" + URL);
			URL url = new URL("http://" + URL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			System.out.println("Output from Peer .... \n");
			while ((output = br.readLine()) != null) {
				//System.out.println(output);
				result = output;
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		
		return result;
	}

}
