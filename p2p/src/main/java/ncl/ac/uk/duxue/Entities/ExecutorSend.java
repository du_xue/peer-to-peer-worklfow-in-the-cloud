package ncl.ac.uk.duxue.Entities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ExecutorSend implements Runnable{

	private String id;
	
	public ExecutorSend(String id )
	{
		this.id = id;
	}
	
	@Override
	public void run() {
		try {

			//URL url = new URL("http://env-8762623.j.layershift.co.uk/peer1" + URL);
			//URL url = new URL("http://localhost:8080/p2p" + URL);
			URL url = new URL("http://localhost:8080/p2p/peer/mutiple/"+id);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			System.out.println("Output from Peer .... \n");
			while ((output = br.readLine()) != null) {
			System.out.println(output);
			
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
	
	}

}
