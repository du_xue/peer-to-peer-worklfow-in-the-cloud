package ncl.ac.uk.duxue.peerservices;

public class Peer {
	private String ID;
	private String groupID;
	private String name;
	private String capacity;
	private String type; 
	private String url;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getGroupID() {
		return groupID;
	}
	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return "Peer [ID=" + ID + ", groupID=" + groupID + ", name=" + name
				+ ", capacity=" + capacity + ", type=" + type + ", url=" + url
				+ "]";
	}

}

