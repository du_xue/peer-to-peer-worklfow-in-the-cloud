package ncl.ac.uk.duxue.queueDBservices;

import java.sql.*;

public class QueueDB {

	public void createQueueTable(String tableName) throws Exception {
		Connection connection = null;
		
		Statement statement = null;

		ResultSet resultSet = null;
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost/p2p?"
							+ "user=root&password=");
			// Statement allow to issue SQL queries to the database
			statement = connection.createStatement();
			// Result set get the result of the SQL query
			String createString = "CREATE TABLE " + tableName
					+ " (uniqueid INT NOT NULL AUTO_INCREMENT, "
					+ "taskID INT NOT NULL," + "processID INT NOT NULL, "
					+ "preProcessID INT ," + "preTaskID INT, "
					+ "poseProcessID INT," + "poseTaskID INT, "
					+ "originalURL VARCHAR(100), " + "fromURL VARCHAR(100),"
					+ "toURL VARCHAR(100)," + "script VARCHAR(400) ,"
					+ "enterTime NUMERIC(20) NOT NULL,"
					+ "PRIMARY KEY (UNIQUEID));";

			statement.execute(createString);
		} catch (Exception e) {
			System.err.println("SQLException:" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}
	}

	public void insertNewTask(String tableName, QTask task)
			throws Exception {
		Connection connection = null;
		Statement statement = null;
		PreparedStatement preparedStatement;
		ResultSet resultSet = null;
		try {
			task.toString();
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost/p2p?"
							+ "user=root&password=");

			// PreparedStatements can use variables and are more efficient
			preparedStatement = connection
					.prepareStatement("insert into p2p."
							+ tableName
							+ " (uniqueid, processID, taskID, preProcessID, preTaskID, poseProcessID, poseTaskID, originalURL, fromURL, toURL, script, enterTime)values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			// Parameters start with 0
			preparedStatement.setInt(1, task.getUniqueID());
			preparedStatement.setInt(2, task.getProcessID());
			preparedStatement.setInt(3, task.getTaskID());
			preparedStatement.setInt(4, task.getPreProcessID());
			preparedStatement.setInt(5, task.getPreTaskID());
			preparedStatement.setInt(6, task.getPoseProcessID());
			preparedStatement.setInt(7, task.getPoseTaskID());
			preparedStatement.setString(8, task.getOriginalURL());
			preparedStatement.setString(9, task.getFromURL());
			preparedStatement.setString(10, task.getToURL());
			preparedStatement.setString(11, task.getScript());
			preparedStatement.setLong(12, task.getEnterTime());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			/* System.err.println("SQLException:" + e.getMessage()); */
			throw e;
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}
	}

	public void deleteTask(String tableName, int uniqueid) {
		Connection connection = null;
		Statement statement = null;
		PreparedStatement preparedStatement;
		ResultSet resultSet = null;
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost/p2p?"
							+ "user=root&password=");
			// Statement allow to issue SQL queries to the database
			statement = connection.createStatement();
			// Result set get the result of the SQL query
			preparedStatement = connection.prepareStatement("delete from p2p."
					+ tableName + " where uniqueid= ? ; ");
			preparedStatement.setInt(1, uniqueid);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			System.err.println("SQLException:" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}

	}

	public void updateTask(String tableName, int uniqueid, long enterTime) {
		Connection connection = null;
		Statement statement = null;
		PreparedStatement preparedStatement;
		ResultSet resultSet = null;
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost/p2p?"
							+ "user=root&password=");
			// Statement allow to issue SQL queries to the database
			statement = connection.createStatement();
			// Result set get the result of the SQL query
			preparedStatement = connection.prepareStatement("UPDATE p2p."
					+ tableName + " SET enterTime= ? where uniqueid= ? ; ");
			preparedStatement.setLong(1, enterTime);
			preparedStatement.setInt(2, uniqueid);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			System.err.println("SQLException:" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}
	}

	public ResultSet getFirstTask(String tableName) {
		Connection connection = null;
		Statement statement1 = null;

		ResultSet resultSet = null;
		// QueueTask task = new QueueTask();
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost/p2p?"
							+ "user=root&password=");
			// Statement allow to issue SQL queries to the database
			statement1 = connection.createStatement();
			// Result set get the result of the SQL query
			String sql = "select uniqueid, processID, taskID, preProcessID, preTaskID, poseProcessID, poseTaskID, originalURL, fromURL, toURL, script, enterTime from "
					+ tableName + " order by enterTime asc";
			resultSet = statement1.executeQuery(sql);
			/*
			 * while (resultSet.next()) {
			 * task.setProcessID(resultSet.getInt(2));
			 * task.setTaskID(resultSet.getInt(3));
			 * task.setPreProcessID(resultSet.getInt(4));
			 * task.setPreTaskID(resultSet.getInt(5));
			 * task.setPoseProcessID(resultSet.getInt(6));
			 * task.setPoseTaskID(resultSet.getInt(7));
			 * task.setOriginalURL(resultSet.getString(8));
			 * task.setFromURL(resultSet.getString(9));
			 * task.setToURL(resultSet.getString(10));
			 * task.setScript(resultSet.getString(11));
			 * task.setEnterTime(resultSet.getLong(12)); task.setUniqueID(); }
			 */
		} catch (Exception e) {
			System.err.println("SQLException:" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement1 != null) {
					statement1.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}
		return resultSet;
	}

	public int getSizeOfQueue(String tableName) {
		Connection connection = null;
		Statement statement = null;

		ResultSet resultSet = null;
		int size = 0;
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost/p2p?"
							+ "user=root&password=");
			// Statement allow to issue SQL queries to the database
			statement = connection.createStatement();
			// Result set get the result of the SQL query
			String sql = "select uniqueid from " + tableName
					+ " order by enterTime asc";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				size++;
			}

		} catch (Exception e) {
			System.err.println("SQLException:" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}
		return size;
	}
}
