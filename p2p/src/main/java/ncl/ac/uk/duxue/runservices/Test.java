package ncl.ac.uk.duxue.runservices;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			
			 
			String content = " sgit ";
            Serializable path = Test.class.getResource("/data.txt");
            System.out.println("this is"+path);
        
            String covertPath = path.toString();
            String s = covertPath.replaceAll("file:/", "");
            System.out.println(s);
			File file = new File(s);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
 
			System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
