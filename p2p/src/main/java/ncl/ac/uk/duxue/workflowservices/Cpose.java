package ncl.ac.uk.duxue.workflowservices;

import java.util.ArrayList;
import java.util.List;

public class Cpose {
	private List<TaskIdentifier> poseList = new ArrayList<TaskIdentifier>();
	private String logic;
	
	public Cpose(){
		
	}

	public List<TaskIdentifier> getPoseList() {
		return poseList;
	}

	public void setPoseList(List<TaskIdentifier> poseList) {
		this.poseList = poseList;
	}

	public String getLogic() {
		return logic;
	}

	public void setLogic(String logic) {
		this.logic = logic;
	}
	
	public void add(TaskIdentifier task)
	{
		poseList.add(task);
	}

	@Override
	public String toString() {
		return "Cpose [poseList=" + poseList + ", logic=" + logic + "]";
	}

	
}
