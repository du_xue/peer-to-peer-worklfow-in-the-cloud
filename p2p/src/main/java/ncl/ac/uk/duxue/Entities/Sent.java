package ncl.ac.uk.duxue.Entities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Sent {
	private String URL;
	private String result;
	
	public Sent(String URL)
	{
		this.URL = URL;
	}
	
	public String clientStart()
	{
		/*try {

			URL url = new URL("http://env-1336410.j.layershift.co.uk/info" + URL);
			//URL url = new URL("http://localhost:8080/info" + URL);
			//URL url = new URL("peer2.elasticbeanstalk.com" + URL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			System.out.println("Output from Peer .... \n");
			while ((output = br.readLine()) != null) {
				//System.out.println(output);
				result = output;
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		
		return result;*/
		
		
		  try {
			  
				//URL url = new URL("http://localhost:8080/RESTfulExample/json/product/get");
			     URL url = new URL("http://localhost:8080/info" + URL);
			    //URL url = new URL("peer2.elasticbeanstalk.com" + URL);
			    //URL url = new URL("http://env-5514763.j.layershift.co.uk/info" + URL);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
		 
				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}
		 
				BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
		 
				String output;
				System.out.println("Output from Peer .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
					result = output;
				}
		 
				conn.disconnect();
		 
			  } catch (MalformedURLException e) {
		 
				e.printStackTrace();
		 
			  } catch (IOException e) {
		 
				e.printStackTrace();
		 
			  }
		      return result;
			}
	     

}
