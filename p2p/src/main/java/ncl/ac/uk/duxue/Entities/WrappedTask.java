package ncl.ac.uk.duxue.Entities;

import java.util.ArrayList;
import java.util.List;

import ncl.ac.uk.duxue.workflowservices.Task;

public class WrappedTask extends Event {
	private String processID;
	private String originalURL;
	private List<String> fromURL = new ArrayList<String>();
	private List<String> toURL = new ArrayList<String>();
	private Task task;

	public WrappedTask() {

	}

	public String getProcessID() {
		return processID;
	}

	public void setProcessID(String processID) {
		this.processID = processID;
	}

	public String getOriginalURL() {
		return originalURL;
	}

	public void setOriginalURL(String originalURL) {
		this.originalURL = originalURL;
	}

	public List<String> getFromURL() {
		return fromURL;
	}

	public void setFromURL(List<String> fromURL) {
		this.fromURL = fromURL;
	}

	public List<String> getToURL() {
		return toURL;
	}

	public void setToURL(List<String> toURL) {
		this.toURL = toURL;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public void addFromURL(String URL) {
		fromURL.add(URL);
	}

	public void addToURL(String URL) {
		toURL.add(URL);
	}

	@Override
	public String toString() {
		return "WrappedTask [processID=" + processID + ", originalURL="
				+ originalURL + ", fromURL=" + fromURL + ", toURL=" + toURL
				+ ", task=" + task + "]";
	}

}
