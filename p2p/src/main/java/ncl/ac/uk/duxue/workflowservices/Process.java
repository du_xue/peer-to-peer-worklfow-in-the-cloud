package ncl.ac.uk.duxue.workflowservices;

import java.util.ArrayList;
import java.util.List;

public class Process 
{
	private String processID;
	private List<Task> taskList =  new ArrayList<Task>();
	
	public Process()
	{
		
	}

	
	public String getProcessID() 
	{
		return processID;
	}

	public void setProcessID(String processID) 
	{
		this.processID = processID;
	}
	
	public void addTask(Task task)
	{
		taskList.add(task);
	}
	public List<Task> getTaskList()
	{
		return taskList;
	}
     
	
}
