package ncl.ac.uk.duxue.Entities;

public abstract class Event {
	  private long eventTime; // the time of this event
	  
	  // Define two methods which all objects will inherit
	  public boolean setEventTime(long val) {
	    eventTime = val;
		return true;
	  }
	  
	  public long getEventTime() {
	    return eventTime;
	  } 
	}