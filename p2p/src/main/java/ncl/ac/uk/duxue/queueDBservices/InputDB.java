/*package ncl.ac.uk.duxue.queueDBservices;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class InputDB {

	public void createInputTable(String tableName)
	{
		 Connection connection = null;
		 Statement statement = null;
		 ResultSet resultSet = null;
		try{
			//This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			//Setup the connection with the DB
			connection =DriverManager.getConnection("jdbc:mysql://localhost/p2p?" + "user=root&password="); 
			//Statement allow to issue SQL queries to the database
			statement = connection.createStatement();
			//Result set get the result of the SQL query
			String createString = "CREATE TABLE "+tableName+" (id INT NOT NULL AUTO_INCREMENT, "+ 
			"taskID INT," +
			"processID INT , "+
            "fromURL VARCHAR(100)," +
            "preTaskID INT," +
			"preProcessID INT , "+
			"data VARCHAR(100)," +
            "PRIMARY KEY (ID));";
			
			statement.execute(createString);
		} catch (Exception e){
			System.err.println("SQLException:" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}
	}
	
	public void insertInput(String tableName, InputRecord input) throws Exception
	{
		 Connection connection = null;
		 Statement statement = null;
		 PreparedStatement preparedStatement;
		 ResultSet resultSet = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			//Setup the connection with the DB
			connection =DriverManager.getConnection("jdbc:mysql://localhost/p2p?" + "user=root&password="); 
			
			//PreparedStatements can use variables and are more efficient
			preparedStatement = connection.prepareStatement("insert into p2p."+tableName+" (id, processID, taskID, fromURL, preProcessID, preTaskID, data)values (?, ?, ?, ?, ?, ?, ?)");
			//Parameters start with 0
			//preparedStatement.setInt(1, input.getId());
			preparedStatement.setInt(2, input.getProcessID());
			preparedStatement.setInt(3, input.getTaskID());
			preparedStatement.setString(4, input.getFromURL());
			preparedStatement.setInt(5, input.getPreProcessID());
			preparedStatement.setInt(6, input.getPreTaskID());
			preparedStatement.setString(7, input.getData());
			preparedStatement.executeUpdate();
		} catch (Exception e){
			throw e;
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}
	}
	
	public void updateInput(String tableName, int preProcessID, int preTaskID, String data) throws Exception
	{
		 Connection connection = null;
		 Statement statement = null;
		 PreparedStatement preparedStatement;
		 ResultSet resultSet = null;
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost/p2p?"
							+ "user=root&password=");
			// Statement allow to issue SQL queries to the database
			statement = connection.createStatement();
			// Result set get the result of the SQL query
			preparedStatement = connection
				      .prepareStatement("UPDATE p2p."+tableName+" SET data= ? where and preProcessID= ? and preTaskID= ?;");
				      preparedStatement.setString(1, data);
				      preparedStatement.setInt(2, preProcessID);
				      preparedStatement.setInt(3, preTaskID);
				      preparedStatement.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}
	}
	public String getInput(String tableName, int processID, int taskID, int preProcessID, int preTaskID)
	{
		 Connection connection = null;
		 Statement statement = null;

		 ResultSet resultSet = null;
		String returnData = null;
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost/p2p?"
							+ "user=root&password=");
			// Statement allow to issue SQL queries to the database
			statement = connection.createStatement();
			// Result set get the result of the SQL query
			String sql = "select data from p2p."+tableName+" where taskID="+taskID+" and processID="+processID+" and preProcessID="+preProcessID+" and preTaskID="+preTaskID;
            resultSet = statement.executeQuery(sql);
			while(resultSet.next())
			{
				returnData = resultSet.getString("data");
			}
		} catch (Exception e) {
			System.err.println("SQLException:" + e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {

			}
		}
		return returnData;
	}
}*/
