package ncl.ac.uk.duxue.queueDBservices;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ncl.ac.uk.duxue.peerservices.Peer;
import ncl.ac.uk.duxue.peerservices.PeerManager;

public class Test {
	static Type collectionType = new TypeToken<List<Peer>>(){}.getType();
	static Gson gson = new Gson();
	static Map<String,List<Peer>> multiMap = new HashMap<String,List<Peer>>();
	public static void main(String[] args) throws Exception
	{
		/*QueueDB queueDB = new QueueDB();
		queueDB.createQueueTable("peer1212task");
		InputDB inputDB = new InputDB();
		inputDB.createInputTable("peer1212input");*/
		/*InputRecord input = new InputRecord();
		input.setFromURL("fromURL");
	
		input.setPreProcessID(213);
		input.setPreTaskID(123);
		input.setTaskID(34);
		input.setProcessID(45);
		
		inputDB.insertInput("peer1212input", input);*/
		/*QueueTask task = new QueueTask();
		task.setEnterTime(123);
		task.setScript("script");
		task.setOriginalURL("bbb");
		task.setFromURL("ff");
		task.setToURL("toURL");
		task.setProcessID(2);
		task.setTaskID(33);
		task.setUniqueID();
		task.toString();
		queueDB.insertNewTask("peer1", task);*/
		//queueDB.updateTask("peer1212task", 233, 12345);
		
		PeerManager peerManager =  new PeerManager();
		String info = peerManager.searchPeer("2311");
		
		//get the peer information from the infopeer search service
		
		List<Peer> peerList = gson.fromJson(info, collectionType);
		System.out.println(peerList);
		multiMap.put("1", peerList);

		for (Entry<String, List<Peer>> entry : multiMap.entrySet()) {
			System.out.println("Key : " + entry.getKey() + " Value : "
				+ entry.getValue());
			if(entry.getValue().isEmpty())
			{
				System.out.println("it is empty");
			}
			else if (entry.getValue()== null)
			{
				System.out.println("it is null");
			}
		}
		
	}

}
