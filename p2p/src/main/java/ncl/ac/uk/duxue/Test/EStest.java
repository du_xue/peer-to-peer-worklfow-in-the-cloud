package ncl.ac.uk.duxue.Test;

import java.io.IOException;

import org.jboss.resteasy.util.Base64;
import ncl.ac.uk.duxue.Entities.SentSide;
import ncl.ac.uk.duxue.workflowservices.Cpose;
import ncl.ac.uk.duxue.workflowservices.Cpre;
import ncl.ac.uk.duxue.workflowservices.Process;
import ncl.ac.uk.duxue.workflowservices.Task;
import ncl.ac.uk.duxue.workflowservices.TaskIdentifier;

import com.google.gson.Gson;


public class EStest implements Runnable {
	private String id;

	public EStest(String id) {
		this.id = id;
	}

	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName() + "Process" + id
				+ " Starts!");
		try {
			TaskIdentifier identifier = new TaskIdentifier();
	        identifier.setProcessId("0");
	        identifier.setTaskID("0");
	        
	        TaskIdentifier identifier1 = new TaskIdentifier();
	        identifier1.setProcessId(id);
	        identifier1.setTaskID("1");
	        
	        TaskIdentifier identifier2 = new TaskIdentifier();
	        identifier2.setProcessId(id);
	        identifier2.setTaskID("2");
	        
	        TaskIdentifier identifier3 = new TaskIdentifier();
	        identifier3.setProcessId(id);
	        identifier3.setTaskID("3");
	        
	        TaskIdentifier identifier4 = new TaskIdentifier();
	        identifier4.setProcessId(id);
	        identifier4.setTaskID("4");
	        
	        TaskIdentifier identifier5 = new TaskIdentifier();
	        identifier5.setProcessId(id);
	        identifier5.setTaskID("5");
	        //Task A
			Cpre pre1 = new Cpre();
			Cpose pose1 = new Cpose();
			
		    pre1.addPre(identifier);
			pre1.setLogic("straight");
			pose1.add(identifier2);
			pose1.add(identifier3);
			pose1.setLogic("and");
			//Task B
			Cpre pre2 = new Cpre();
			Cpose pose2 = new Cpose();
			
			pre2.addPre(identifier1);
			pre2.setLogic("straight");
			pose2.add(identifier4);
			pose2.setLogic("straight");
			//Task C
			Cpre pre3 = new Cpre();
			Cpose pose3 = new Cpose();
			
			pre3.addPre(identifier1);
			pre3.setLogic("straight");
			pose3.add(identifier4);
			pose3.setLogic("straight");
			//Task D
			Cpre pre4 = new Cpre();
			Cpose pose4 = new Cpose();
			
			pre4.addPre(identifier2);
			pre4.addPre(identifier3);
			pre4.setLogic("and");
			pose4.add(identifier5);
			pose4.setLogic("straight");
			//Task E
			Cpre pre5 = new Cpre();
			Cpose pose5 = new Cpose();
			
			pre5.addPre(identifier4);
			pre5.setLogic("straight");
			pose5.add(identifier);
			pose5.setLogic("straight");
			
			Task taskA = new Task("1", "A", pre1, pose1, "Tech", "writefile.exe");
			Task taskB = new Task("2", "B", pre2, pose2, "Math", "writefile.exe");
			Task taskC = new Task("3", "C", pre3, pose3, "Math", "writefile.exe");
			Task taskD = new Task("4", "D", pre4, pose4, "Phy", "writefile.exe");
			Task taskE = new Task("5", "E", pre5, pose5, "Eng", "writefile.exe");
	  
			Process process = new Process();
			process.setProcessID(id);

			process.addTask(taskA);
			process.addTask(taskB);
			process.addTask(taskC);
			process.addTask(taskD);
			process.addTask(taskE);
			
			////////////////////////////////////////////////////////Test 2
		/*	TaskIdentifier identifier = new TaskIdentifier();
	        identifier.setProcessId("0");
	        identifier.setTaskID("0");
			
			// Task A
			Cpre pre1 = new Cpre();
			Cpose pose1 = new Cpose();

			pre1.addPre(identifier);
			pre1.setLogic("straight");
			pose1.add(identifier);
			pose1.add(identifier);
			pose1.setLogic("straight");

			Process process = new Process();
			process.setProcessID(id);

			Task taskA = new Task("1", "A", pre1, pose1, "Math", "writefile.exe");
			process.addTask(taskA);*/
			

			//////////////////////////////////////////////////////////////////////////Test 3
		/*	TaskIdentifier identifier = new TaskIdentifier();
	        identifier.setProcessId("0");
	        identifier.setTaskID("0");
	        
	            //Task A
	      		Cpre pre1 = new Cpre();
	      		Cpose pose1 = new Cpose();
	      		
	      	    pre1.addPre(identifier);
	      		pre1.setLogic("and");
	      		pose1.add(identifier);
	      		pose1.add(identifier);
	      		pose1.setLogic("and");
	      		//Task B
	      		Cpre pre2 = new Cpre();
	      		Cpose pose2 = new Cpose();
	      		
	      		pre2.addPre(identifier);
	      		pre2.setLogic("and");
	      		pose2.add(identifier);
	      		pose2.setLogic("and");
	      		//Task C
	      		Cpre pre3 = new Cpre();
	      		Cpose pose3 = new Cpose();
	      		
	      		pre3.addPre(identifier);
	      		pre3.setLogic("and");
	      		pose3.add(identifier);
	      		pose3.setLogic("and");
	      		//Task D
	      		Cpre pre4 = new Cpre();
	      		Cpose pose4 = new Cpose();
	      		
	      		pre4.addPre(identifier);
	      		pre4.addPre(identifier);
	      		pre4.setLogic("and");
	      		pose4.add(identifier);
	      		pose4.setLogic("and");
	      		//Task E
	      		Cpre pre5 = new Cpre();
	      		Cpose pose5 = new Cpose();
	      		
	      		pre5.addPre(identifier);
	      		pre5.setLogic("and");
	      		pose5.add(identifier);
	      		pose5.setLogic("and");
	      		
	         	//Task F
	      		Cpre pre6 = new Cpre();
	      		Cpose pose6 = new Cpose();
	      		
	      		pre6.addPre(identifier);
	      		pre6.setLogic("and");
	      		pose6.add(identifier);
	      		pose6.setLogic("and");
			

	         	//Task G
	      		Cpre pre7 = new Cpre();
	      		Cpose pose7 = new Cpose();
	      		
	      		pre7.addPre(identifier);
	      		pre7.setLogic("and");
	      		pose7.add(identifier);
	      		pose7.setLogic("and");
	      		

	         	//Task H
	      		Cpre pre8 = new Cpre();
	      		Cpose pose8 = new Cpose();
	      		
	      		pre8.addPre(identifier);
	      		pre8.setLogic("and");
	      		pose8.add(identifier);
	      		pose8.setLogic("and");
	      		

	         	//Task I
	      		Cpre pre9 = new Cpre();
	      		Cpose pose9 = new Cpose();
	      		
	      		pre9.addPre(identifier);
	      		pre9.setLogic("and");
	      		pose9.add(identifier);
	      		pose9.setLogic("and");
	      		

	         	//Task J
	      		Cpre pre10 = new Cpre();
	      		Cpose pose10 = new Cpose();
	      		
	      		pre10.addPre(identifier);
	      		pre10.setLogic("and");
	      		pose10.add(identifier);
	      		pose10.setLogic("and");
	      		

	         	//Task K
	      		Cpre pre11 = new Cpre();
	      		Cpose pose11 = new Cpose();
	      		
	      		pre11.addPre(identifier);
	      		pre11.setLogic("and");
	      		pose11.add(identifier);
	      		pose11.setLogic("and");
	      		
	      	   //Task L
	      		Cpre pre12 = new Cpre();
	      		Cpose pose12 = new Cpose();
	      		
	      		pre12.addPre(identifier);
	      		pre12.setLogic("and");
	      		pose12.add(identifier);
	      		pose12.setLogic("and");
	      		
	      		Process process = new Process();
	    		process.setProcessID(id);
	    		
	    		Task taskA = new Task("1", "A", pre1, pose1, "Eng", "writefile.exe");
	    		Task taskB = new Task("2", "B", pre2, pose2, "Eng", "writefile.exe");
	    		Task taskC = new Task("3", "C", pre3, pose3, "Eng", "writefile.exe");
	    		Task taskD = new Task("4", "D", pre4, pose4, "Eng", "writefile.exe");
	    		Task taskE = new Task("5", "E", pre5, pose5, "Eng", "writefile.exe");
	    		Task taskF = new Task("6", "F", pre6, pose6, "Eng", "writefile.exe");
	    		Task taskG = new Task("7", "G", pre7, pose7, "Eng", "writefile.exe");
	    		Task taskH = new Task("8", "H", pre8, pose8, "Eng", "writefile.exe");
	    		Task taskI = new Task("9", "I", pre9, pose9, "Eng", "writefile.exe");
	    		Task taskJ = new Task("10", "J", pre10, pose10, "Eng", "writefile.exe");
	    		Task taskK = new Task("11", "K", pre11, pose11, "Eng", "writefile.exe");
	    		Task taskL = new Task("12", "L", pre12, pose12, "Eng", "writefile.exe");

	    		process.addTask(taskA);
	    		process.addTask(taskB);
	    		process.addTask(taskC);
	    	    process.addTask(taskD);
	    		process.addTask(taskE);
	    		process.addTask(taskF);
	    	 */
	      		

			Gson gson = new Gson();

			String json = gson.toJson(process);
			System.out.println(json);
			
		    String base64Encoded = Base64.encodeObject(json, Base64.URL_SAFE);

			SentSide client = new SentSide("localhost:8080/p2p/peer/workflow/"
					+ base64Encoded);
			String status = client.clientStart();

			System.out.println(Thread.currentThread().getName() + "Process"
					+ id + " Ends!");
			System.out.println(status);
			System.out.println("workflow!!!"+base64Encoded);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
