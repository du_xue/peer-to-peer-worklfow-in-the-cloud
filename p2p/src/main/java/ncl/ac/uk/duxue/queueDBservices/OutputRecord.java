package ncl.ac.uk.duxue.queueDBservices;

public class OutputRecord {
	private String identifier;
	private int processID;
	private int taskID;
	private String toURL;
	private int poseProcessID;
	private int poseTaskID;
	private String data;
	
	public String getIdentifier()
	{
		return identifier;
	}
	public void setIdentifier()
	{
		String processid = ""+poseProcessID;
		String taskid = ""+poseTaskID;
		identifier = processid+"."+taskid;
	}
	public int getProcessID() 
	{
		return processID;
	}
	public void setProcessID(int processID) 
	{
		this.processID = processID;
	}
	public int getTaskID() 
	{
		return taskID;
	}
	public void setTaskID(int taskID) 
	{
		this.taskID = taskID;
	}
	public String getToURL() 
	{
		return toURL;
	}
	public void setToURL(String toURL) 
	{
		this.toURL = toURL;
	}
	public int getPoseProcessID() 
	{
		return poseProcessID;
	}
	public void setPoseProcessID(int poseProcessID) 
	{
		this.poseProcessID = poseProcessID;
	}
	public int getPoseTaskID()
	{
		return poseTaskID;
	}
	public void setPoseTaskID(int poseTaskID)
	{
		this.poseTaskID = poseTaskID;
	}
	public String getData()
	{
		return data;
	}
	public void setData(String data)
	{
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "OutputRecord [identifier=" + identifier + ", processID="
				+ processID + ", taskID=" + taskID + ", toURL=" + toURL
				+ ", poseProcessID=" + poseProcessID + ", poseTaskID="
				+ poseTaskID + ", data=" + data + "]";
	}
	
	

}
