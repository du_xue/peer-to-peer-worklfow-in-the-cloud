package ncl.ac.uk.duxue.workflowservices;

public class Task {
	private String taskID;
	private String taskName;
	private Cpre pre;
	private Cpose pose;
	private String type;
	private String specifics;
	
	public Task(String taskID, String taskName, Cpre pre, Cpose pose,
			String type, String specifics) 
	{
		this.taskID = taskID;
		this.taskName = taskName;
		this.pre = pre;
		this.pose = pose;
		this.type = type;
		this.specifics = specifics;
	}
	public String getTaskName() 
	{
		return taskName;
	}
	public void setTaskName(String taskName)
	{
		this.taskName = taskName;
	}
	public String getTaskID() 
	{
		return taskID;
	}
	public void setTaskID(String taskID) 
	{
		this.taskID = taskID;
	}
	public String getType() 
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public String getSpecifics() 
	{
		return specifics;
	}
	public void setSpecifics(String specifics)
	{
		this.specifics = specifics;
	}
	public Cpre getPre() 
	{
		return pre;
	}
	public void setPre(Cpre pre) 
	{
		this.pre = pre;
	}
	public Cpose getPose() 
	{
		return pose;
	}
	public void setPose(Cpose pose) 
	{
		this.pose = pose;
	}
	@Override
	public String toString() 
	{
		return "Task [taskID=" + taskID + ", taskName=" + taskName + ", pre="
				+ pre + ", pose=" + pose + ", type=" + type + ", specifics="
				+ specifics + "]";
	}
	
	
}
