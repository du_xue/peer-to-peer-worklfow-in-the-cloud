package ncl.ac.uk.duxue.peerservices;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.util.Base64;

import ncl.ac.uk.duxue.Entities.Message;
import ncl.ac.uk.duxue.Entities.WrappedTask;
import ncl.ac.uk.duxue.workflowservices.Task;
import ncl.ac.uk.duxue.workflowservices.TaskIdentifier;



import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Path("/peer")
public class PeerResource {
    
	String result = null;
	Gson gson = new Gson();
	Peer peer = new Peer();
	static private String record;
    String id = "1";
    
    Type collectionType = new TypeToken<List<String>>(){}.getType();
    Type collectionType2 = new TypeToken<List<TaskIdentifier>>(){}.getType();
    
    Worker worker = new Worker();
    PeerManager peerManager = new PeerManager();
	
	@GET
	@Path("/")
	public Response testREST() throws IOException
	{
		
		return Response.ok("Call Success!").build();
	}
	
	@GET
	@Path("/information")
	public Response testGetInfo()
	{
		return Response.ok("Call Success!!"+"\n"+record).build();
	}
	
	@GET
	@Path("/workflow/{workflow}")

	public Response submitWorkflow(@PathParam("workflow") String workflow) throws IOException, ClassNotFoundException, InterruptedException
	{
	
		try {
			System.out.println((String)Base64.decodeToObject(workflow));
			result = (String)Base64.decodeToObject(workflow);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		
		ncl.ac.uk.duxue.workflowservices.Process process = gson.fromJson(result, ncl.ac.uk.duxue.workflowservices.Process.class);
		System.out.println();
		
		peerManager = new PeerManager();
		String status = peerManager.changeJsontoObjec(process);
		
		return Response.ok(status).build();
	}
	
	@GET
	@Path("/message")
	public Response getTaskResquest()
	{
		/*int status = 0;
		status = 100;*/
	    //worker = Worker.getInstance();
		int status = worker.getSize();
		return Response.ok(status).build();
	}
	
	@POST
	@Path("/wrappedTask")
	public Response getWrappedTask(@FormParam("processid") String processid,
			                       @FormParam("fromurl") String fromurlJson,
			                       @FormParam("tourl") String tourlJson,
			                       @FormParam("originalurl") String originalurl,
			                       @FormParam("task") String taskJson,
			                       @FormParam("prelist") String preListJson,
			                       @FormParam("poselist") String poseListJson) throws InterruptedException, IOException, ClassNotFoundException
	{
		/*int status = 0;
		status = 100;*/
		//result = (String)Base64.decodeToObject(wrappedTask);
		/*WrappedTask wrapped = gson.fromJson(wrappedTask, WrappedTask.class);
		System.out.println("@ + "+wrapped);

		System.out.println("!" + wrapped + "!");
		String status = worker.onWork(wrapped);*/
		Task taskObject = gson.fromJson(taskJson, Task.class);
		ArrayList<String> fromurl = gson.fromJson(fromurlJson, collectionType);
		ArrayList<String> tourl = gson.fromJson(tourlJson, collectionType);
		ArrayList<TaskIdentifier> prelist = gson.fromJson(preListJson, collectionType2);
		ArrayList<TaskIdentifier> poselist = gson.fromJson(poseListJson, collectionType2);
		
		WrappedTask wrappedTask = new WrappedTask();
		wrappedTask.setProcessID(processid);
		wrappedTask.setFromURL(fromurl);
		wrappedTask.setToURL(tourl);
		wrappedTask.setOriginalURL(originalurl);
		wrappedTask.setTask(taskObject);
		taskObject.getPre().setPreList(prelist);
		taskObject.getPose().setPoseList(poselist);
		//worker = Worker.getInstance();
		String status = worker.onWork(wrappedTask);
	
		record = record+"\n****************************************"+"\n"+"\n"+status;
		//System.out.println("@inside restful status + "+status);
		//return Response.ok(status + "\n" + ":ProcessID: "+wrapped.getProcessID()+" TaskID: "+wrapped.getTask().getTaskID()+"<=== from ownpeer -received").build();
		return Response.ok("STATUS!"+"\n"+status).build();
	}
	
	@POST
	@Path("/datamessage")
	public Response getMessage(@FormParam("processid") int processid,
			                   @FormParam("taskid") int taskid,
			                   @FormParam("data") String data ) throws Exception
	{
		Message message= new Message();
		message.setData(data);
		message.setProcessID(processid);
		message.setTaskID(taskid);
		
		worker.receiveMessage(message);
		return Response.ok("message from @ "+message.getProcessID()+"."+message.getTaskID()+" @ ===> "+message.toString()+"inside ownpeer -received").build();
	}
		
}
