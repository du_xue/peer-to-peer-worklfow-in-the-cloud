package ncl.ac.uk.duxue.Entities;


public class Workload extends Event{
	
	private String peerItemID;
	
	public Workload(String peerItemID, int workload)
	{
		this.peerItemID = peerItemID;
		super.setEventTime(workload);
	}
	
	public String getItemID()
	{
		return peerItemID;
	}

}
