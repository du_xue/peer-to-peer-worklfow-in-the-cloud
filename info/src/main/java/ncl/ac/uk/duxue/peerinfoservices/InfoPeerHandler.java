package ncl.ac.uk.duxue.peerinfoservices;

import ncl.ac.uk.duxue.Entities.Peer;

public interface InfoPeerHandler {
	
	public void joinPeer(Peer peer);      // add a new peer's information
	
	public String searchPeer(String type)throws Exception;    // find a peer's information
	
	public void deletePeer(String peerID);    // delete a peer's information
	
	public String updatePeer(String itemID, String attribute, String value);    // update a peer's information

}
