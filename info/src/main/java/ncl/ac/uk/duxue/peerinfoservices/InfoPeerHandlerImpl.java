package ncl.ac.uk.duxue.peerinfoservices;

import ncl.ac.uk.duxue.Entities.Peer;
import ncl.ac.uk.duxue.simpleDBservices.PeerDBHandler;
import ncl.ac.uk.duxue.simpleDBservices.PeerDBHandlerImpl;

public class InfoPeerHandlerImpl implements InfoPeerHandler{
	
	/*
	 * Implementation of new peer joining information
	 */
	public void joinPeer(Peer peer)
	{
		PeerDBHandler dataStore = new PeerDBHandlerImpl();
		dataStore.add(peer);
	}
	
	/*
	 * Implementation of a peer leaving information
	 */
	public void deletePeer(String peerID)
	{
		PeerDBHandler dataDelete = new PeerDBHandlerImpl();
		dataDelete.delete(peerID);
	}
	
	/*
	 * Implementation of searching a peer's information
	 */
	public String searchPeer(String type) throws Exception
	{
		PeerDBHandler dataSearch = new PeerDBHandlerImpl();
		String string = dataSearch.search(type).toString();
		return string;
	}
	
	/*
	 * Implementation of update information of a peer
	 */
	public String updatePeer(String itemID, String attribute, String value)
	{
		System.out.println(itemID + "\n" + attribute +"\n" + value);
		PeerDBHandler dataUpdate = new PeerDBHandlerImpl();
		String string = dataUpdate.update(itemID, attribute, value);
		return string;
	}
	

}
