package ncl.ac.uk.duxue.Entities;

public class Message {
	private int processID;
	private int taskID;
	private String data;
	public int getProcessID() 
	{
		return processID;
	}
	public void setProcessID(int processID) 
	{
		this.processID = processID;
	}
	public int getTaskID() 
	{
		return taskID;
	}
	public void setTaskID(int taskID) 
	{
		this.taskID = taskID;
	}
	public String getData() 
	{
		return data;
	}
	public void setData(String data) 
	{
		this.data = data;
	}
	@Override
	public String toString()
	{
		return "Message [processID=" + processID + ", taskID=" + taskID
				+ ", data=" + data + "]";
	}
	
	

}
