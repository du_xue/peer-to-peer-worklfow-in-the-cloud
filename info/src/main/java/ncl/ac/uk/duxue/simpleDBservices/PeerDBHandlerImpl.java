package ncl.ac.uk.duxue.simpleDBservices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ncl.ac.uk.duxue.Entities.Peer;


import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.BatchPutAttributesRequest;
import com.amazonaws.services.simpledb.model.CreateDomainRequest;
import com.amazonaws.services.simpledb.model.DeleteAttributesRequest;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.ReplaceableItem;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.google.gson.Gson;

public class PeerDBHandlerImpl implements PeerDBHandler{
	 private static AmazonSimpleDBClient sdb;
	    private static final String DOMAIN= "infopeerS";
	 

	// -------------------------- Static methods --------------------------

	    static {
	        try {
	            sdb = new AmazonSimpleDBClient(
	                    new PropertiesCredentials(PeerDBHandlerImpl.class.getResourceAsStream("/AwsCredentials.properties")));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        sdb.setEndpoint("sdb.eu-west-1.amazonaws.com");
	        sdb.createDomain(new CreateDomainRequest(DOMAIN));
	       
	    }
	/*
	 *constructor 
	 */
	
	public PeerDBHandlerImpl()
	{
		
			
	}
	/*interface method*/
	public void add(Peer peer)
	{
		sdb.batchPutAttributes(new BatchPutAttributesRequest(DOMAIN, createNewPeerInfo(peer)));
	}
	
	public void delete(String peerID)
	{
		sdb.deleteAttributes(new DeleteAttributesRequest(DOMAIN, peerID));
	}
	
	public String update(String itemID, String attribute, String value)
	{
		System.out.println(itemID + "\n" + attribute +"\n" + value);
		List<ReplaceableAttribute> replaceableAttributes = new ArrayList<ReplaceableAttribute>();
		replaceableAttributes.add(new ReplaceableAttribute(attribute, value, true));
		sdb.putAttributes(new PutAttributesRequest(DOMAIN, itemID, replaceableAttributes));
		String string = "successful!";
		return string;
	}
	
	public String search(String type) throws Exception
	{
		String  json = null;
		try {
		String selectExpression = "select * from `" + DOMAIN + "` where type = '"+ type +"'";
		//System.out.println(type+"22");
		 SelectRequest selectRequest = new SelectRequest(selectExpression);
		 List<Item> resultItem = sdb.select(selectRequest).getItems();
		 List<Peer> resultPeer = new ArrayList<Peer>();
		 for (Item item : resultItem)
		 {
			 Peer peer = new Peer();
			 for (Attribute attribute : item.getAttributes())
			 {
				 String string = attribute.getName();
				/* switch(string)
				 {
				 case "ID":
					 peer.setID(attribute.getValue());
				 case "groupID":
					 peer.setGroupID(attribute.getValue());
				 case "name":
					 peer.setName(attribute.getValue());
				 case "capacity":
					 peer.setCapacity(attribute.getValue());
				 case "type":
					 peer.setType(attribute.getValue());
				 case "url":
					 peer.setUrl(attribute.getValue());
				 default:
					 break;
				 }*/
				 if(string.equals("ID"))
				 {
					 peer.setID(attribute.getValue());
				 }
				 else if(string.equals("groupID"))
				 {
					 peer.setGroupID(attribute.getValue());
				 }
				 else if(string.equals("name"))
				 {
					 peer.setName(attribute.getValue());
				 }
				 else if(string.equals("capacity"))
				 {
					 peer.setCapacity(attribute.getValue());
				 }
				 else if(string.equals("type"))
				 {
					 peer.setType(attribute.getValue());
				 }
				 else if(string.equals("url"))
				 {
					 peer.setUrl(attribute.getValue());
				 }
			 }
			resultPeer.add(peer);
		 }
		 
		 /*System.out.println(item.toString());*/
		 Gson gson = new Gson();
		 json = gson.toJson(resultPeer);
		// base64Encoded  = Base64.encodeObject(json, Base64.URL_SAFE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(12000);
		 return json;
		 
	}
	/*
	 *  create an array of peer's information
	 *  @return An array of new peer information
	*/
	private static List<ReplaceableItem> createNewPeerInfo(Peer peer)
	{
		List<ReplaceableItem> peerInfoData = new ArrayList<ReplaceableItem>();
		String peerID = peer.getGroupID()+peer.getID();
		
		peerInfoData.add(new ReplaceableItem(peerID).withAttributes(
				new ReplaceableAttribute("ID", peer.getID(), true),
				new ReplaceableAttribute("groupID", peer.getGroupID(), true),
				new ReplaceableAttribute("name", peer.getName(), true),
				new ReplaceableAttribute("type", peer.getType(), true),
				new ReplaceableAttribute("capacity", peer.getCapacity(), true),
				new ReplaceableAttribute("url", peer.getUrl(), true)
				));
		return peerInfoData;
	}
	
}
