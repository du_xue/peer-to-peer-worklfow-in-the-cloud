package ncl.ac.uk.duxue.peerinfoservices;

import javax.ws.rs.core.Response;

import ncl.ac.uk.duxue.Entities.Peer;

public class InfoPeerResourceImpl implements InfoPeerResource {
	
	public Response createPeer(String ID, String groupID, String name, String capacity, String type, String url)
	{
		Peer newPeer = new Peer();
		newPeer.setID(ID);
		newPeer.setGroupID(groupID);
		newPeer.setName(name);
		newPeer.setCapacity(capacity);
		newPeer.setType(type);
		newPeer.setUrl(url);
		
		InfoPeerHandler infoPeer = new InfoPeerHandlerImpl(); 
		infoPeer.joinPeer(newPeer);
	   /* PeerDBHandler data = new PeerDBHandlerService();
		data.add(newPeer);*/
		
		return Response.ok(newPeer.toString()).build();

	}
	
	public String getPeer(String type ) throws Exception{
		InfoPeerHandler updatePeer = new InfoPeerHandlerImpl(); 
		String string= updatePeer.searchPeer(type);
		
		return string;
		
	}
	
	public Response updatePeer(String ID, String groupID, String attribute, String value)
	{
		String itemID = groupID + ID;
		System.out.println("IPRS" + itemID + "\n" + attribute +"\n" + value);
		InfoPeerHandler infoPeer = new InfoPeerHandlerImpl();
		String string = infoPeer.updatePeer(itemID, attribute, value);
		return Response.ok(string).build();
	}
	
	public void deletePeer(String id)
	{
		InfoPeerHandler infoPeer = new InfoPeerHandlerImpl();
		infoPeer.deletePeer(id);
	}

}
