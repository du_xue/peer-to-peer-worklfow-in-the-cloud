package ncl.ac.uk.duxue.peerinfoservices;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.*;



@Path("/infopeer")
public interface InfoPeerResource {
		
	@POST
	@Path("/join")
	public Response createPeer(@FormParam("ID") String ID, 
			                   @FormParam("groupID") String groupID,
			                   @FormParam("name") String name,
			                   @FormParam("capacity") String capacity,
			                   @FormParam("type") String type,
			                   @FormParam("url") String url
			                  );
	
	@GET
	@Path("/search/{type}")

	public String getPeer(@PathParam("type") String type)throws Exception;
	
	@POST
	@Path("/update")

	public Response updatePeer(@FormParam("ID") String ID, 
			                   @FormParam("groupID") String groupID,
			                   @FormParam("attribute") String attribute,
			                   @FormParam("value") String value);
	
	@DELETE
	@Path("/leave/{id}")
	public void deletePeer(@PathParam("id") String id);
	
}
