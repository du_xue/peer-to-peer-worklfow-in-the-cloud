package ncl.ac.uk.duxue.simpleDBservices;



import ncl.ac.uk.duxue.Entities.Peer;

public interface PeerDBHandler {
	
	public void add(Peer peer);
	
	public void delete(String peerID);
	
	public String update(String itemID, String attribute, String value);
	
	public String search(String type)throws Exception;

}
